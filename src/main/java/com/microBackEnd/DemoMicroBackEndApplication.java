package com.microBackEnd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMicroBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoMicroBackEndApplication.class, args);
	}

}
